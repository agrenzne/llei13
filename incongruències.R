#======================================================================================#
#========================= (6) ERRORS / INCONGRUČNCIES ================================#
#======================================================================================#

setwd(paste0(path_project, "scripts/Incongručncies"))

write.csv(Ealtes,"Ealtes.csv", fileEncoding="utf8")
write.csv(Ebaixes,"Ebaixes.csv", fileEncoding="utf8")
write.csv(Emodif,"Emodif.csv", fileEncoding="utf8")

EOrdUnicsNB<-subset(ordin02NB, duplicated(ordin02NB$EXPEDIENT)==TRUE)  # [err]
if(nrow(EOrdUnicsNB)!=0){
  rownames(EOrdUnicsNB)<-c(1:nrow(EOrdUnicsNB))
}
write.csv(EOrdUnicsNB,"EduplicatsNB.csv", fileEncoding="utf8") # ??